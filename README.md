# MD 2017 History Fork

Hey, there! Thank you for your interest in wanting to work on MD's 2017 start date. This is entirely open-sourced which means it is now in the hands of you the players of MD if you wish to see more accurate Order of Battles.
MD originally used the book The Military Balance for ours so if you wish to have similar OoB structures to current MD I'd recommend using that book.

Thanks, Bird. 

We as the team do not provide any guarantees about how to make this run simply providing you the files. Please create a fork and continue your work from there! 
